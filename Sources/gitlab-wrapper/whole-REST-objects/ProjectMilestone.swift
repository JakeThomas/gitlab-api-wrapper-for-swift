import Foundation

open class ProjectMilestone: Milestone {
    let project_id: Int

    enum CodingKeys: String, CodingKey {
        case project_id
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        project_id = try values.decode(Int.self, forKey: .project_id)
        try! super.init(from: decoder)
    }
}
