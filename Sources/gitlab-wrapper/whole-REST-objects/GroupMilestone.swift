import Foundation

open class GroupMilestone: Milestone {
    let group_id: Int
    let web_url: URL

    enum CodingKeys: String, CodingKey {
        case group_id
        case web_url
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        group_id = try values.decode(Int.self, forKey: .group_id)
        web_url = try values.decode(URL.self, forKey: .web_url)
        try! super.init(from: decoder)
    }
}
