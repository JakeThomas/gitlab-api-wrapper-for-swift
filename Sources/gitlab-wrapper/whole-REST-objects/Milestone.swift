import Foundation

open class Milestone: WholeRestObject {
    public let id: Int
    let iid: Int
    let title: String
    let description: String?
    let due_date: Date?
    let start_date: Date?
    let state: State
    let updated_at: Date
    let created_at: Date
    let expired: Bool
    
    enum State: String, Decodable {
        case active
        case closed
    }

    enum CodingKeys: String, CodingKey {
        case id
        case iid
        case title
        case description
        case due_date
        case start_date
        case state
        case updated_at
        case created_at
        case expired
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        iid = try values.decode(Int.self, forKey: .iid)
        title = try values.decode(String.self, forKey: .title)
        description = try values.decode(String?.self, forKey: .description)
        
        if let due_date_str = try values.decode(String?.self, forKey: .due_date) {
            due_date = storageForThisFile.dateFormatter.date(from:
                due_date_str
            )
        } else {
            due_date = nil
        }
        if let start_date_str = try values.decode(String?.self, forKey: .start_date) {
            start_date = storageForThisFile.dateFormatter.date(from:
                start_date_str
            )
        } else {
            start_date = nil
        }
        
        state = try values.decode(State.self, forKey: .state)
        updated_at = try values.decode(Date.self, forKey: .updated_at)
        created_at = try values.decode(Date.self, forKey: .created_at)
        expired = try values.decode(Bool.self, forKey: .expired)
    }
}

fileprivate struct StorageForThisFile {
    let dateFormatter = DateFormatter()
    init() {
        dateFormatter.dateFormat = "yyyy-MM-dd"
    }
}
fileprivate let storageForThisFile = StorageForThisFile()
