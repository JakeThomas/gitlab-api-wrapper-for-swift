// To maintain, use an "example response" from the doc:
// https://docs.gitlab.com/ee/api/issues.html
// Also include any Premium and Ultimate features that
// might not be in any "example response".
// Just search the page for "Premium" and "Ultimate".

import Foundation
open class Issue: WholeRestObject {
    
    // Attributes are listed in this file in the same order
    // as seen in an example response.
    let project_id: Int
    let milestone: Milestone?
    let author: User
    let description: String?
    let weight: Int?
    let epic: Epic?
    let iteration: Iteration?
    let health_status: HealthStatus?
    let state: IssueState
    let iid: Int
    let assignees: [User]
    // let assignee: User? -- Deprecated *and* redundant
    // let type: String -- Redundant with class name
    let labels: [String]
    let upvotes: Int
    let downvotes: Int
    let merge_request_count: Int
    public let id: Int
    let moved_to_id: Int?
    let title: String
    let updated_at: Date
    let created_at: Date
    let closed_at: Date?
    let closed_by: User?
    let user_notes_count: Int
    let due_date: Date?
    let web_url: URL
    let references: References
    let time_stats: TimeStats
    let has_tasks: Bool
    let task_status: String?
    let confidential: Bool
    let discussion_locked: Bool
    let issue_type: IssueType
    let severity: Severity
    let _links: Links
    let task_completion_status: TaskCompletionStatus
    
    enum HealthStatus: String, Decodable {
        case on_track
        case needs_attention
        case at_risk
    }
    
    enum IssueState: String, Decodable {
        case opened
        case closed
    }
    
    struct References: Decodable {
      let short: String
      let relative: String
      let full: String
    }
    
    struct TimeStats: Decodable {
        let time_estimate: Int
        let total_time_spent: Int
        let human_time_estimate: String?
        let human_total_time_spent: String?
    }
    
    enum IssueType: String, Decodable {
        case issue
        case incident
        case test_case
    }
    
    // Source: https://docs.gitlab.com/ee/user/application_security/vulnerabilities/severities.html
    enum Severity: String, Decodable {
        case critical
        case high
        case medium
        case low
        case info
        case unknown
        
        // Credit: https://sarunw.com/posts/how-to-decode-enums-ignoring-case-in-swift-codable
        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            let rawString = try container.decode(String.self)
            
            if let severity = Severity(rawValue: rawString.lowercased()) {
                self = severity
            } else {
                throw DecodingError.dataCorruptedError(
                    in: container,
                    debugDescription: "Cannot initialize Severity from invalid String value \(rawString)."
                )
            }
        }
    }
    
    struct Links: Decodable {
        let `self`: URL
        let notes: URL
        let award_emoji: URL
        let project: URL
        let closed_as_duplicate_of: URL
    }
    
    struct TaskCompletionStatus: Decodable {
        let count: Int
        let completed_count: Int
    }
}
