public protocol WholeRestObject: Hashable, Decodable {
    var id: Int { get }
}

public extension WholeRestObject {
    static func ==(lhs: Self, rhs: Self) -> Bool {
         lhs.id == rhs.id && type(of: lhs) == type(of: rhs)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(String(reflecting: type(of: self)))
    }
}
