import Foundation

class TimeZoneCache {
    static private var cache: [TimeZoneEnum : TimeZone] = [:]
    
    static func get(_ timeZoneEnum: TimeZoneEnum) -> TimeZone {
        let timeZone = cache[timeZoneEnum]
        
        guard let timeZone = timeZone else {
            let newTimeZone = TimeZone(abbreviation: timeZoneEnum.rawValue)!
            cache[timeZoneEnum] = newTimeZone
            return newTimeZone
        }
        
        return timeZone
    }
}
