import Foundation

fileprivate struct StorageForThisFile {
    let dateFormatter = DateFormatter()
    init() {
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}
fileprivate let storageForThisFile = StorageForThisFile()

extension JSONDecoder {
    convenience init(usingTimeZone timeZone: TimeZone) {
        self.init()
        storageForThisFile.dateFormatter.timeZone = timeZone
        self.dateDecodingStrategy = .formatted(storageForThisFile.dateFormatter)
    }
}

extension JSONDecoder {
    func decode<T>(_ type: T.Type, from string: String) throws -> T where T: Decodable {
        try! decode(type, from: string.data(using: .utf8)!)
    }
}
