## Extending `Issue`, `Epic`, etc.
The library does not get or remotely store additional attributes for you at time-of-writing.  
Maybe you stand up a NoSQL server. Maybe you commit json files.

Example: add a `cost` attribute to `Issue` where `cost` may or may not have been assigned to the issue yet,
out on the server(s):

```swift
class BudgetedIssue: Issue {
    let cost: Int? // <------------------- Note the `?`
    init(from decoder: Decoder){
        // code to grab custom attribute(s), just `cost` in this example.
        cost = thingThatWasGrabbed.cost // Or something like that
        super.init(from: decoder)
    }
}
```
* Inherits `WholeRestObject` conformance from `Issue` and therefore `Decodable` and `Hashable` conformance as well
* Equality and hashing provided by default implementations in `WholeRestObject`
  * Determined by dynamic type and `id`. All `WholeRestObject`s have an `id`.
    * So a plain `Issue` with an `id` of `38` and a `BudgetedIssue` with an `id` of `38` would *not* equal. 
    
Example: add a `cost` attribute to `Issue` where `cost` has already been obtained:

```swift
class BudgetedIssue: Issue {
    let cost: Int // <------------------- Note the *lack* of `?`
    init(from decoder: Decoder){
        // Below, `GlobalThing` is a global struct or class with a `static` attribute, `costs`.
        // `costs` is a dictionary of `Int`s (the Issue `id`s) to `Int`s (cost in dollars or other currency).
        cost = GlobalThing.costs[self.id] // Or something like that
        super.init(from: decoder)
    }
}
```
